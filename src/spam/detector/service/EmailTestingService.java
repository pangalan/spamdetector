package spam.detector.service;

import spam.detector.observer.Observer;
import spam.detector.observer.Subject;
import spam.detector.processor.SpamProcessorCreator;
import spam.detector.processor.TestingProcessor;
import spam.detector.processor.bayes.testing.ContextHolder;
import spam.detector.source.Email;

import java.util.stream.Collectors;

public class EmailTestingService extends Subject implements Observer {

    private static final EmailTestingService instance = new EmailTestingService();

    public static EmailTestingService getInstance() {
        return instance;
    }

    private final TestingProcessor testingProcessor;
    private final EmailStorageService emailStorageService = EmailStorageService.getInstance();

    private EmailTestingService() {
        testingProcessor = SpamProcessorCreator.getInstance().createTestingProcessor();
    }

    private void test(Iterable<Email> emails) {
        emails.forEach(email -> email.setSpam(testingProcessor.test(email.getSource())));
        notifyObserver();
    }

    @Override
    public void update(Subject subject) {
        if (subject instanceof EmailStorageService) {
            test(emailStorageService.findAll().stream().filter(email -> !email.hasTested()).collect(Collectors.toList()));
            return;
        }
        if (subject instanceof EmailTrainingService || subject instanceof ContextHolder) {
            test(emailStorageService.findAll());
        }
    }
}
