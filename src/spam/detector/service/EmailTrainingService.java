package spam.detector.service;

import spam.detector.observer.Subject;
import spam.detector.processor.SpamProcessorCreator;
import spam.detector.processor.TrainingProcessor;
import spam.detector.source.Source;

import java.util.Collection;

public class EmailTrainingService extends Subject {

    private static final EmailTrainingService instance = new EmailTrainingService();

    public static EmailTrainingService getInstance() {
        return instance;
    }

    private final TrainingProcessor trainingProcessor;

    private EmailTrainingService() {
        trainingProcessor = SpamProcessorCreator.getInstance().createTrainingProcessor();
    }

    public void trainHam(Collection<Source> sources) {
        trainingProcessor.trainHam(sources.toArray(new Source[]{}));
        notifyObserver();
    }

    public void trainSpam(Collection<Source> sources) {
        trainingProcessor.trainSpam(sources.toArray(new Source[]{}));
        notifyObserver();
    }
}
