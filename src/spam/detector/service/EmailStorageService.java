package spam.detector.service;

import spam.detector.observer.Subject;
import spam.detector.source.Email;
import spam.detector.source.EmailRepository;
import spam.detector.source.EmailStorage;
import spam.detector.source.Source;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EmailStorageService extends Subject {

    private static EmailStorageService instance = new EmailStorageService();

    public static EmailStorageService getInstance() {
        return instance;
    }

    private final EmailRepository emailRepository = EmailStorage.getInstance();

    private EmailStorageService() {}

    public void addEmail(File... files) {
        emailRepository.addAll(Arrays.stream(files)
                                     .map(file -> new Email(new Source(file), file.getName()))
                                     .collect(Collectors.toList()));
        notifyObserver();
    }

    public List<Email> findAll() {
        return emailRepository.findAll();
    }

    public void clear() {
        emailRepository.clear();
    }

}
