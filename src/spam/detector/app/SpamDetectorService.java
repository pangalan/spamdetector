package spam.detector.app;

import spam.detector.processor.bayes.testing.Context;
import spam.detector.processor.bayes.testing.ContextHolder;
import spam.detector.service.EmailStorageService;
import spam.detector.service.EmailTrainingService;
import spam.detector.source.Email;
import spam.detector.source.Source;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SpamDetectorService {

    private static final SpamDetectorService instance = new SpamDetectorService();

    public static SpamDetectorService getInstance() {
        return instance;
    }

    private SpamDetectorService() {}

    private final EmailStorageService emailStorageService = EmailStorageService.getInstance();
    private final EmailTrainingService emailTrainingService = EmailTrainingService.getInstance();
    private final ContextHolder contextHolder = ContextHolder.getInstance();

    public void addEmail(File... files) {
        emailStorageService.addEmail(files);
    }

    public List<Email> findAll() {
        return emailStorageService.findAll();
    }

    public void trainHam(File ... files) {
        emailTrainingService.trainHam(Arrays.stream(files)
                                            .map(Source::new)
                                            .collect(Collectors.toList()));
    }

    public void trainSpam(File ... files) {
        emailTrainingService.trainSpam(Arrays.stream(files)
                                            .map(Source::new)
                                            .collect(Collectors.toList()));
    }

    public void changeSpamBound(double bound) {
        contextHolder.setContext(new Context(bound));
    }

    public void clearMail() {
        emailStorageService.clear();
    }


}
