package spam.detector.app.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import spam.detector.app.SpamDetectorService;
import spam.detector.observer.Observer;
import spam.detector.observer.Subject;
import spam.detector.service.EmailStorageService;
import spam.detector.service.EmailTestingService;
import spam.detector.source.Email;

import java.io.File;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;

public class MainController implements Observer {

    private static final String SPAM_TITLE = "Спам (%s)";
    private static final String HAM_TITLE = "Почта (%s)";

    private final SpamDetectorService service = SpamDetectorService.getInstance();
    private final FileChooser fileChooser = new FileChooser();
    private final DirectoryChooser directoryChooser = new DirectoryChooser();
    @FXML
    private RadioMenuItem perc25;
    @FXML
    private RadioMenuItem perc50;
    @FXML
    private RadioMenuItem perc75;
    @FXML
    private TitledPane spamPane;
    @FXML
    private TitledPane hamPane;
    @FXML
    private ListView<Email> spamList;
    @FXML
    private ListView<Email> hamList;
    @FXML
    private WebView emailView;
    @FXML
    private ImageView hamImage;
    @FXML
    private ImageView spamImage;


    @FXML
    private void initialize() {
        hamImage.setVisible(false);
        spamImage.setVisible(false);
        directoryChooser.setInitialDirectory(new File(getClass().getResource("/data").getFile()));
        fileChooser.setInitialDirectory(new File(getClass().getResource("/data").getFile()));
        Stream.of(spamList, hamList)
              .forEach(emailListView -> {
                  emailListView.setCellFactory(new Callback<ListView<Email>, ListCell<Email>>() {
                      @Override
                      public ListCell<Email> call(ListView<Email> param) {
                          return new ListCell<Email>() {
                              @Override
                              protected void updateItem(Email item, boolean empty) {
                                  super.updateItem(item, empty);
                                  if (item != null) {
                                      this.setText(item.getName());
                                  }
                              }
                          };
                      }
                  });
                  emailListView.getSelectionModel()
                               .selectedItemProperty()
                               .addListener((observable, oldValue, newValue) -> updateEmailView(newValue));
              });
        ToggleGroup group = new ToggleGroup();
        group.getToggles().addAll(perc25, perc50, perc75);
        group.selectedToggleProperty()
             .addListener((observable, oldValue, newValue) -> service.changeSpamBound(calcBound(newValue)));
        perc50.setSelected(true);
    }

    private double calcBound(Toggle toggle) {
        if (!(toggle instanceof RadioMenuItem)) {
            return 0.5;
        }
        String text = ((RadioMenuItem) toggle).getText();
        return 1 - Double.parseDouble(text.split("%")[0]) / 100.0;
    }

    public void openMailFile() {
        fileChooser.setTitle("Выберите файл с почтой");
        File file = openFile();
        if (file != null) {
            service.clearMail();
            extractEmail(file);
        }
    }

    public void openMailDirectory() {
        directoryChooser.setTitle("Выберите папку с почтой");
        File file = openFolder();
        if (file != null) {
            service.clearMail();
            extractEmail(file);
        }
    }

    public void trainSpam() {
        service.trainSpam(retrieveDataset());
    }

    public void trainHam() {
        service.trainHam(retrieveDataset());
    }

    private File[] retrieveDataset() {
        directoryChooser.setTitle("Выберите папку с данными");
        File file = openFolder();
        List<File> dataset = new ArrayList<>();
        if (file != null) {
            extractDataset(file, dataset);
        }
        return dataset.toArray(new File[]{});
    }

    private void extractDataset(File file, Collection<File> dataset) {
        Map<Boolean, List<File>> fileGroups = Arrays.stream(Objects.requireNonNull(file.listFiles()))
                                                    .collect(groupingBy(File::isDirectory));
        List<File> files = fileGroups.get(false);
        if (files != null) {
            dataset.addAll(files);
        }
        List<File> directories = fileGroups.get(true);
        if (directories != null) {
            directories.forEach(each -> extractDataset(each, dataset));
        }
    }

    @Override
    public void update(Subject subject) {
        if (subject instanceof EmailStorageService || subject instanceof EmailTestingService) {
            updateEmails();
        }
    }

    private void updateEmailView(Email email) {
        if (email == null) {
            return;
        }
        emailView.getEngine().loadContent(email.getSource().getContent().replace("\n", "<br>"));
        if (email.hasTested()) {
            spamImage.setVisible(email.isSpam());
            hamImage.setVisible(!email.isSpam());
        }
    }

    private void updateEmails() {
        Map<Boolean, List<Email>> emailGroups = service.findAll()
                                                       .stream()
                                                       .collect(groupingBy(email -> email.hasTested() && email.isSpam()));
        updateList(emailGroups.get(true), spamPane, spamList, SPAM_TITLE);
        updateList(emailGroups.get(false), hamPane, hamList, HAM_TITLE);
    }

    private void updateList(List<Email> mail, TitledPane pane, ListView<Email> listView, String title) {
        listView.getItems().clear();
        if (mail == null) {
            pane.setText(String.format(title, 0));
            return;
        }
        pane.setText(String.format(title, mail.size()));
        listView.setItems(FXCollections.observableArrayList(mail));
    }

    private void extractEmail(File file) {
        if (!file.isDirectory()) {
            service.addEmail(file);
            return;
        }
        Map<Boolean, List<File>> groups = Arrays.stream(Objects.requireNonNull(file.listFiles()))
                                                .collect(groupingBy(File::isDirectory));
        List<File> hams = groups.get(false);
        if (hams != null) {
            service.addEmail(hams.toArray(new File[]{}));
        }
        List<File> spams = groups.get(true);
        if (spams != null) {
            spams.forEach(this::extractEmail);
        }
    }

    private File openFile() {
        return fileChooser.showOpenDialog(null);
    }

    private File openFolder() {
        return directoryChooser.showDialog(null);
    }
}
