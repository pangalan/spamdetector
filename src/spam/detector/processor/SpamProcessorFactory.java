package spam.detector.processor;

public interface SpamProcessorFactory {

    TrainingProcessor createTrainingProcessor();

    TestingProcessor createTestingProcessor();

}
