package spam.detector.processor;

import spam.detector.source.Source;

public interface TrainingProcessor {

    void trainSpam(Source ... source);

    void trainHam(Source ... source);

}
