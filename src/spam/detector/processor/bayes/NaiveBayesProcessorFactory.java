package spam.detector.processor.bayes;

import spam.detector.processor.SpamProcessorFactory;
import spam.detector.processor.TestingProcessor;
import spam.detector.processor.TrainingProcessor;
import spam.detector.processor.bayes.training.NaiveBayesTrainingProcessor;

public class NaiveBayesProcessorFactory implements SpamProcessorFactory {

    @Override
    public TrainingProcessor createTrainingProcessor() {
        return new NaiveBayesTrainingProcessor();
    }

    @Override
    public TestingProcessor createTestingProcessor() {
        return null;
    }
}
