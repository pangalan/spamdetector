package spam.detector.processor.bayes.training;

public class HamFrequencyHolder extends FrequencyHolder {
    private static HamFrequencyHolder ourInstance = new HamFrequencyHolder();

    public static HamFrequencyHolder getInstance() {
        return ourInstance;
    }

    private HamFrequencyHolder() {
    }
}
