package spam.detector.processor.bayes.training;

import java.util.HashMap;
import java.util.Map;

public class FrequencyHolder {

    private final Map<String, Double> frequency = new HashMap<>();
    private final Map<String, Integer> words = new HashMap<>();

    public void reset() {
        frequency.clear();
        words.clear();
    }

    public void addWord(String word) {
        if (!words.containsKey(word)) {
            words.put(word, 1);
            return;
        }
        words.replace(word, words.get(word) + 1);
    }

    public void countFrequencies(int total) {
        words.forEach((word, count) -> frequency.put(word, countFrequency(count, total)));
    }

    public Map<String, Double> getFrequency() {
        return frequency;
    }

    private double countFrequency(int count, int total) {
        return count * 1.0 / total;
    }

}
