package spam.detector.processor.bayes.training;

import spam.detector.source.Source;
import spam.detector.observer.Subject;
import spam.detector.processor.TrainingProcessor;

import java.util.HashSet;
import java.util.Set;

public class NaiveBayesTrainingProcessor extends Subject implements TrainingProcessor {

    public NaiveBayesTrainingProcessor() {
        attach(SpamWordsDetector.getInstance());
    }

    @Override
    public void trainSpam(Source ... source) {
        train(SpamFrequencyHolder.getInstance(), source);
        notifyObserver();
    }

    @Override
    public void trainHam(Source ... source) {
        train(HamFrequencyHolder.getInstance(), source);
        notifyObserver();
    }

    private void train(FrequencyHolder holder, Source ... source) {
        for (Source mail : source) {
            Set<String> words = new HashSet<>();
            while(mail.hasNext()) {
                words.add(mail.next());
            }
            words.forEach(holder::addWord);
            holder.countFrequencies(source.length);
        }
    }

}
