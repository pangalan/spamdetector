package spam.detector.processor.bayes.training;

import spam.detector.observer.Observer;
import spam.detector.observer.Subject;
import spam.detector.processor.bayes.SpamWordStorage;

public class SpamWordsDetector implements Observer {

    private static final SpamWordsDetector instance = new SpamWordsDetector();

    public static SpamWordsDetector getInstance() {
        return instance;
    }

    private SpamWordsDetector() {}

    private final SpamWordStorage spamWordStorage = SpamWordStorage.getInstance();
    private final SpamFrequencyHolder spamFrequencyHolder = SpamFrequencyHolder.getInstance();
    private final HamFrequencyHolder hamFrequencyHolder = HamFrequencyHolder.getInstance();

    @Override
    public void update(Subject subject) {
        spamFrequencyHolder.getFrequency().entrySet()
                           .stream()
                           .filter(wordFreq -> hamFrequencyHolder.getFrequency().containsKey(wordFreq.getKey()))
                           .forEach(wordFreq -> spamWordStorage.add(wordFreq.getKey(),
                                                                    count(wordFreq.getValue(),
                                                                          hamFrequencyHolder.getFrequency()
                                                                                            .get(wordFreq.getKey()))));
    }

    private double count(double spamFrequency, double hamFrequency) {
        return spamFrequency / (spamFrequency + hamFrequency);
    }
}
