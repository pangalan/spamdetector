package spam.detector.processor.bayes.training;

public class SpamFrequencyHolder extends FrequencyHolder {
    private static SpamFrequencyHolder ourInstance = new SpamFrequencyHolder();

    public static SpamFrequencyHolder getInstance() {
        return ourInstance;
    }

    private SpamFrequencyHolder() {
    }
}
