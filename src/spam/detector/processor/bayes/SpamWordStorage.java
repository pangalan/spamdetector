package spam.detector.processor.bayes;

import java.util.HashMap;
import java.util.Map;

public class SpamWordStorage {

    private static SpamWordStorage instance = new SpamWordStorage();

    public static SpamWordStorage getInstance() {
        return instance;
    }

    private final Map<String, Double> spamWords = new HashMap<>();

    private SpamWordStorage() {
    }

    public boolean contains(String word) {
        return spamWords.containsKey(word);
    }

    public Double getProbability(String word) {
        return spamWords.get(word);
    }

    public void add(String word, Double probability) {
        spamWords.put(word , probability);
    }
}
