package spam.detector.processor.bayes.testing;

import spam.detector.source.Source;

public class SpamDetectorExpert {

    private final ProbabilityCounter counter = new ProbabilityCounter();

    public Boolean isSpam(Source source, Context context) {
        return countProbability(source) > (context.getProbabilityBound() + context.getPrecision());
    }

    private double countProbability(Source source) {
        double n = 0.0;
        source.reset();
        while (source.hasNext()) {
            n += counter.count(source.next());
        }
        return counter.count(n);
    }

}
