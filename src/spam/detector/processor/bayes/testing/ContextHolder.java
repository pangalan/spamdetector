package spam.detector.processor.bayes.testing;

import spam.detector.observer.Subject;

public class ContextHolder extends Subject {
    private static ContextHolder instance = new ContextHolder();

    public static ContextHolder getInstance() {
        return instance;
    }

    private Context context;

    private ContextHolder() {
        context = new Context();
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
        notifyObserver();
    }
}
