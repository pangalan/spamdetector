package spam.detector.processor.bayes.testing;

import spam.detector.source.Source;
import spam.detector.processor.TestingProcessor;

public class NaiveBayesTestingProcessor implements TestingProcessor {

    private final SpamDetectorExpert detectorExpert = new SpamDetectorExpert();
    private final ContextHolder contextHolder = ContextHolder.getInstance();

    @Override
    public Boolean test(Source source) {
        return detectorExpert.isSpam(source, contextHolder.getContext());
    }
}
