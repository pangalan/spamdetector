package spam.detector.processor.bayes.testing;

public class Context {

    private static final double PRECISION = 1e-5;
    private static final double PROBABILITY_BOUND = 0.5;

    private final double probabilityBound;
    private final double precision;

    public Context(double probabilityBound) {
        this.probabilityBound = probabilityBound;
        this.precision = PRECISION;
    }

    public Context() {
        precision = PRECISION;
        probabilityBound = PROBABILITY_BOUND;
    }

    public double getPrecision() {
        return precision;
    }

    public double getProbabilityBound() {
        return probabilityBound;
    }
}
