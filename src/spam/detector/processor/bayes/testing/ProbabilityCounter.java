package spam.detector.processor.bayes.testing;

import spam.detector.processor.bayes.SpamWordStorage;

public class ProbabilityCounter {

    private final SpamWordStorage spamWordStorage = SpamWordStorage.getInstance();

    public Double count(String word) {
        if (spamWordStorage.contains(word)) {
            Double probability = spamWordStorage.getProbability(word);
            return Math.log((1 - probability - Math.log(probability)));
        }
        return 0.0;
    }

    public Double count(double value) {
        return 1 / (1 + Math.pow(Math.E, value));
    }

}
