package spam.detector.processor;

public class SpamAlgorithmDefiner implements AlgorithmDefiner {

    private static SpamAlgorithmDefiner instance = new SpamAlgorithmDefiner();

    public static SpamAlgorithmDefiner getInstance() {return instance;}

    private SpamAlgorithmDefiner() {}

    @Override
    public AlgorithmType define() {
        String property = System.getProperty("alg", "NAIVE_BAYER");
        try {
            return AlgorithmType.valueOf(property);
        } catch (IllegalArgumentException e) {
            return AlgorithmType.UNKNOWN;
        }
    }
}
