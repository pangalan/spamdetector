package spam.detector.processor;

import spam.detector.exception.WrongAlgorithmException;
import spam.detector.processor.bayes.testing.NaiveBayesTestingProcessor;
import spam.detector.processor.bayes.training.NaiveBayesTrainingProcessor;
import spam.detector.processor.random.RandomTestingProcessor;
import spam.detector.processor.random.RandomTrainingProcessor;

public class SpamProcessorCreator implements SpamProcessorFactory {

    private final static SpamProcessorCreator instance = new SpamProcessorCreator();

    public static SpamProcessorCreator getInstance() {
        return instance;
    }

    private SpamProcessorCreator(){}

    private final AlgorithmDefiner algorithmDefiner = SpamAlgorithmDefiner.getInstance();

    @Override
    public TrainingProcessor createTrainingProcessor() {
        switch (algorithmDefiner.define()) {
            case NAIVE_BAYER:
                return new NaiveBayesTrainingProcessor();
            case RANDOM:
                return new RandomTrainingProcessor();
            default:
                throw new WrongAlgorithmException();
        }
    }

    @Override
    public TestingProcessor createTestingProcessor() {
        switch (algorithmDefiner.define()) {
            case NAIVE_BAYER:
                return new NaiveBayesTestingProcessor();
            case RANDOM:
                return new RandomTestingProcessor();
            default:
                throw new WrongAlgorithmException();
        }
    }

}
