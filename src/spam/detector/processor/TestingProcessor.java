package spam.detector.processor;

import spam.detector.source.Source;

public interface TestingProcessor {

    Boolean test(Source source);

}
