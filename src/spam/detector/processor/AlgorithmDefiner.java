package spam.detector.processor;

public interface AlgorithmDefiner {

    AlgorithmType define();

}
