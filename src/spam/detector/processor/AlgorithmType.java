package spam.detector.processor;

public enum AlgorithmType {

    NAIVE_BAYER, RANDOM, UNKNOWN

}
