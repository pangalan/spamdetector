package spam.detector.processor.random;

import spam.detector.processor.TestingProcessor;
import spam.detector.source.Source;

import java.util.Random;

public class RandomTestingProcessor implements TestingProcessor {

    private static final Random RANDOM = new Random();

    @Override
    public Boolean test(Source source) {
        return RANDOM.nextBoolean();
    }
}
