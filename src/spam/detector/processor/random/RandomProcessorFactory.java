package spam.detector.processor.random;

import spam.detector.processor.SpamProcessorFactory;
import spam.detector.processor.TestingProcessor;
import spam.detector.processor.TrainingProcessor;

public class RandomProcessorFactory implements SpamProcessorFactory {

    @Override
    public TrainingProcessor createTrainingProcessor() {
        return null;
    }

    @Override
    public TestingProcessor createTestingProcessor() {
        return null;
    }
}
