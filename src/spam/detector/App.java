package spam.detector;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import spam.detector.observer.AttachService;

public class App extends Application {

    private final AttachService attachService = new AttachService();

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/form/main.fxml"));
        Parent root = fxmlLoader.load();
        attachService.attach(fxmlLoader.getController());
        primaryStage.setTitle("Spam Detector");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
