package spam.detector.observer;

import spam.detector.app.controller.MainController;
import spam.detector.processor.bayes.testing.ContextHolder;
import spam.detector.service.EmailStorageService;
import spam.detector.service.EmailTestingService;
import spam.detector.service.EmailTrainingService;

public class AttachService {

    public void attach(MainController controller) {
        EmailStorageService.getInstance().attach(EmailTestingService.getInstance());
        EmailTrainingService.getInstance().attach(EmailTestingService.getInstance());
        ContextHolder.getInstance().attach(EmailTestingService.getInstance());
        EmailStorageService.getInstance().attach(controller);
        EmailTestingService.getInstance().attach(controller);
    }

}
