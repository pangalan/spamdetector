package spam.detector.source;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Source {

    private Scanner scanner;
    private String content;

    public Source(File file) {
        try {
            scanner = new Scanner(file);
            read();
            reset();
        } catch (FileNotFoundException e) {
            throw new spam.detector.exception.FileNotFoundException(file.getPath());
        }
    }

    public Source(String string) {
        this.scanner = new Scanner(string);
        content = string;
    }

    public String next() {
        return scanner.next();
    }

    public boolean hasNext() {
        return scanner.hasNext();
    }

    public void reset() {
        scanner = new Scanner(content);
    }

    public String getContent() {
        return content;
    }

    private void read() {
        StringBuilder builder = new StringBuilder();
        while(scanner.hasNext()) {
            builder.append(scanner.nextLine()).append("\n");
        }
        content = builder.toString();
    }
}
