package spam.detector.source;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EmailStorage implements EmailRepository {
    private static EmailStorage instance = new EmailStorage();

    public static EmailStorage getInstance() {
        return instance;
    }

    private final List<Email> emails = new ArrayList<>();
    private long idCounter = 0;

    private EmailStorage() {
    }

    @Override
    public List<Email> findAll() {
        return emails;
    }

    @Override
    public void add(Email email) {
        emails.add(email);
        email.setId(idCounter++);
    }

    @Override
    public void addAll(Collection<Email> emails) {
        this.emails.addAll(emails);
        emails.forEach(email -> email.setId(idCounter++));
    }

    @Override
    public void clear() {
        emails.clear();
    }
}
