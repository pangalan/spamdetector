package spam.detector.source;

import java.util.Collection;
import java.util.List;

public interface EmailRepository {

    void add(Email email);
    void addAll(Collection<Email> emails);
    List<Email> findAll();

    void clear();


}
