package spam.detector.source;

public class Email {

    private final Source source;
    private long id;
    private Boolean isSpam;
    private String name = "No name";

    public Email(Source source) {
        this.source = source;
    }

    public Email(Source source, String name) {
        this.source = source;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Source getSource() {
        return source;
    }

    public boolean hasTested() {
        return isSpam != null;
    }

    public Boolean isSpam() {
        return isSpam;
    }

    public void setSpam(boolean spam) {
        isSpam = spam;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
