package spam.detector.exception;

public class SpamDetectorException extends RuntimeException {

    public SpamDetectorException(String message, String ... arguments) {
        super(String.format(message, arguments));
    }
}
