package spam.detector.exception;

public class WrongAlgorithmException extends SpamDetectorException {

    private static final String MESSAGE = "В программе отсутствует реализация запрашиваемого алгоритма";

    public WrongAlgorithmException() {
        super(MESSAGE);
    }
}
