package spam.detector.exception;

public class FileNotFoundException extends SpamDetectorException {

    private static final String MESSAGE = "Файл не найден %s";

    public FileNotFoundException(String path) {
        super(MESSAGE, path);
    }
}
